/*
 * Fucking limit is unsupported right now.
 * Will update later.
 */
exports.findUserByName = function(userId, collegeId, name, page, cb){
	User.find(
		{
			'$and': [
				{
					collegeId: collegeId
				},

				{
					'$or': [
						{
							firstname: new RegExp(name, "i")
						},
						{
							lastname: new RegExp(name, "i")
						}
					]
				}
			]
		},
		{
			firstname: 1,
			lastname: 1
		}).exec(function(err, users){
		if(err){
			sails.log.error(err);
			return cb(err, null);
		}

		return cb(null, users);
	});
}

exports.getTop = function(categoryId, cId, page, total, cb){
	Profile.find({where: {categoryId: categoryId, collegeId: cId}, sort: 'upvoteCount desc'}).paginate({page: page, limit: total}).exec(function(err, prifiles){

		if(err){
			sails.log.info(err);
			return cb(err, null);
		}

		if(!prifiles|| prifiles.length == 0)
			return cb(null, []);

		var data = [];
		for(var i=0; i<prifiles.length; i++){
			(function(i, data){
				var temp = {};
				prifiles[i].getUserDetail(function(err, user){
					if(err)
						return cb(err, null);
					temp.id = prifiles[i].id;
					temp.categoryId = prifiles[i].categoryId;
					temp.upvoteCount = prifiles[i].upvoteCount;
					temp.userId = user.id;
					temp.user = user;
					data.push(temp);

					checkgetTop(data, prifiles.length-1, function(err, ans){
						if(ans)
							return cb(null, ans);
					});
				});
			})(i, data);
		}
	});
}

/* Same as topall, who cares.*/
checkgetTop = function(data, expected, cb){
	if(data.length > expected)
		return cb(null, data);
	else
		return cb(data, null);
}

exports.getTopFromAllCategory = function(collegeId, cb){
	var data = {};
	College.findOne({id: collegeId}).exec(function(err, college){
		if(err){
			sails.log.info(err);
			return cb(err, null);
		}

		if(!college)
			return cb({errorMessage: "This college does not exists", errorCode: "NOT_EXIST"}, null);

		data.college = college;
		data.profile = [];
		Category.find({where: {}, sort: 'title desc'}).exec(function(err, category){
			if(err){
				sails.log.info(err);
				return cb(err, null);
			}

			if(!category)
				return cb({errorMessage: "No category not exists", errorCode: "NOT_EXIST"}, null);

			// sails.log.info(category.length);
			for(var i=0; i<category.length; i++){
				(function(i, data){
					UserService.getTop(category[i].id, collegeId, 1, 1, function(err, profile){
						if(err)
							return cb(err, null);
						if(profile.length == 0){
							var temp = {};
							temp.categoryId = category[i].id;
							temp.categoryTitle = category[i].title;
							//temp.user = [];
							data.profile.push(temp);
						}
						else
							data.profile.push(profile[0]);

						// sails.log.info(data.profile.length);	
						topall(data, category.length-1, function(err, ans){
							if(ans)
								return cb(null, ans);
						});
					});
				})(i, data);
			}
		});
	});
}

topall = function(data, expected, cb){
	/* TODO: Something is fishy in this line. */
	if(data.profile.length > expected)
		return cb(null, data);
	else
		return cb(data, null);
}

exports.getUserPublicProfile = function(id, cb){
	User.findOne({id: id}).exec(function(err, user){
		if(err){
			sails.log.info(err);
			return cb(err, null);
		}

		if(!user)
			return cb({errorMessage: "No such user", errorCode: "NOT_EXIST"}, null);

		return cb(null, user.publicProfile());
	});
}