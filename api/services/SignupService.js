var username = "nkmann2@gmail.com";
var password = "bbACOMdDBDXY1rKI65mNEw";
var crypto = require('crypto');
var mailer = require('mailer');
var bcrypt = require('bcrypt');

sendEmailConfirmation = function(email, code, userId, cb){
	var finalText = 'http://54.254.171.50/user/confirmation/'+ userId.toString()+ '/'+ code.toString()+'"';
    mailer.send({host: "smtp.mandrillapp.com", 
        port: 587, 
        to: email.toString(),
        from: "me@nameless.in",
        subject: "Email confirmation",
        html: 'Click at the link to verify your account! <a href="'+finalText+'">'+finalText+'</a>',
        text: 'Click at the link to verify your account! '+finalText,
        authentication: "login",
        username: username,
        password: password
    }, function(err, result){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        return cb(null, result);
    });
}

sendPwdResetEmail = function(email, code, userId, cb){
    mailer.send({host: "smtp.mandrillapp.com", 
        port: 587, 
        to: email.toString(),
        from: "me@nameless.in",
        subject: "Password reset!",
        body: "Change your password here! http://54.254.171.50/user/resetting/"+ userId.toString()+ "/"+ code.toString(),
        authentication: "login",
        username: username,
        password: password
    }, function(err, result){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        return cb(null, result);
    });
}

exports.signUpUser = function(username, password, email, firstname, lastname, collegeId, gcmRegistrationId, cb){
	User.findOne({'or': [ { username: username }, { email: email } ]}).exec(function(errF, existingUser){
		if(errF) {
			sails.log.error(errF);
			return cb({errorMessage: "Unable to find user", errorCode: "DATABASE_ERROR"},null);
		}
		if(existingUser!=null){

			if(existingUser.email == email)
				return cb({errorMessage: "Email has already taken", errorCode: "DUPLICATE_EMAIL"}, null);

			if(existingUser.username == username)
				return cb({errorMessage: "username has already taken", errorCode: "DUPLICATE_USERNAME"}, null);

			else
				return cb({errorMessage: "error", errorCode: "UNKNOWN_ERROR"}, null);
		}

		College.findOne({id: collegeId}).exec(function(err, college){

			if(err){
				sails.log.error(err);
				return cb(err, null);
			}

			if(!college)
				return cb({errorMessage: "This college is not defined", errorCode: "VALIDATION_ERROR"}, null);

			if(email.indexOf(college.emailIdFormat) == -1)
				return cb({errorMessage: "email does not belog to this college", errorCode: "VALIDATION_ERROR"}, null);

			User.create({username: username, email: email, password: password, firstname: firstname, lastname: lastname, collegeName:college.shortCode, gcmRegistrationId: gcmRegistrationId, collegeId: college.id}, function(err, result){

				if(err) {
					sails.log.error(err);
					return cb(err, null);
				}

				crypto.randomBytes(16, function(err, buf) {
					if(err){
						sails.log.error(err);
						return cb(err, null, null, null);
					}

					var emailCode = buf.toString('hex');
					Verification.create({userId: result.id, utility: 0, code: emailCode}).exec(function(err, verify){
						if(err){
							sails.log.error("Database error for verificatino token saving");
							sails.log.error(err);
						}

						sails.log.info("Verification token has been created");
						sendEmailConfirmation(email, result.id, emailCode, function(err, ver){
							if(err) {
								sails.log.error("Error sending confirmation email to "+email);
								sails.log.error(err);
							}
							else
								sails.log.info("Confirmation email sent to "+email);
						});
					});
				});

				delete result.password;
				delete result.createdAt;
				delete result.updatedAt;
				sails.log.info("User account created");
				return cb(null, result);
			});
		});
	})
}

exports.signin = function(username, password, cb){

	var email_reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(email_reg.test(username))
		var userToFind = {'email': username};
	else
		var userToFind = {'username': username};

	User.findOne(userToFind).exec(function(err, result){
		if(err) {
			sails.log.error(err);
			return cb({errorMessage:"cannot locate user",errorCode:"DATABASE_ERROR"},null);
		}

		if(!result) {
			return cb({errorMessage:"No such user !!",errorCode:"USER_NOT_FOUND"}, null);
		}

		var hash = result.password;
		bcrypt.compare(password, hash, function(errInP,result1){
			if(errInP) {
				sails.log.error(errInP);
				return cb({errorMessage: "cannot retreive password", errorCode: "DATABASE_ERROR"});
			}
			if(!result1){
				sails.log.info("Wrong password for user "+username);
				return cb({errorMessage:"Wrong password !!",errorCode:"PASSWORD_MISMATCH"},null);
			}
			
			if(!result.isEmailVerified){
				sails.log.info("Email unverified");
				return cb({errorMessage: "Your email is not verified.", errorCode: "ACCESS_DENIED"}, null);
			}

			crypto.randomBytes(16, function(err, buf) {
				if(err){
					sails.log.error(err);
					return cb(err, null, null, null);
				}

				var token = buf.toString('hex');
				RedisService.set(token, result.id, result.collegeId, function(err, isset){
					if(err){
						sails.log.error(err);
						return cb({errorMessage: "Error occured, try again.", errorCode: "REDIS_ERROR"}, null);
					}

					result["token"] = token;
					return cb(null, result.publicProfile());
				});
			});
		});
	});
}


exports.verifyEmail = function(userId, token, cb){
	Verification.findOne({userId: userId, utility:0, code: token}).exec(function(err, suc){
		if(err) {
			sails.log.error(err);
			return cb(err, null);
		}

		if(!suc)
			return cb({errorMessage: "Unidentified token!! Expired link.", errorCode: "ACCESS_DENIED"});

		var createdAt = new Date(suc.createdAt).getTime()/1000;
		var curr = new Date().getTime()/1000;

		//24hr expiration!
		// if(curr - createdAt > 86400)
		// 	return cb({errorMessage: "Expired link, request new confirmation email.", errorCode: "ACCESS_DENIED"});

		User.update({id: userId}, {____verify__Email__True: sails.config.checker.secretCode, isEmailVerified: true}).exec(function(err, user){
			if(err) {
				sails.log.error(err);
				return cb(err, null);
			}

			// sails.log.info("updating user here too :)");
			Verification.destroy({userId: userId, utility: 0}).exec(function(err, des){
				if(err) {
					sails.log.error(err);
					return cb(err, null);
				}

				return cb(null, "Verified");
			});
		});
	});
}

exports.sendResetEmail = function(username, cb){
	var email_reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(email_reg.test(username))
		var userToFind = {'email': username};
	else
		var userToFind = {'username': username};

	User.findOne(userToFind).exec(function(err, user){
		if(err) {
			sails.log.error(err);
			return cb(err, null);
		}

		if(!user){
			return cb({errorMessage: "Unidentified user!!", errorCode: "USER_NOT_FOUND"});
		}

		if(!user.email)
			return cb({errorMessage: "you cannot reset your password!!", errorCode: "EMAIL_NOT_FOUND"});

		crypto.randomBytes(16, function(err, buf){
			if(err){
				sails.log.error(err);
				return cb(err, null, null, null);
			}

			var code = buf.toString('hex');
			Verification.destroy({userId: user.id, utility: 2}).exec(function(err, deleted){
				if(err){
					sails.log.error(err);
					return cb(err, null, null, null);
				}

				Verification.create({userId: user.id, code: code, utility: 2}, function(err, ver){
					sendPwdResetEmail(user.email, code, user.id, function(err, em){
						if(err) {
							sails.log.error(err);
							// return cb(err, null);
						}
					});

					return cb(null , {status: true});
				});
			});
		});
	});
}

exports.resetUserPassword = function(userId, token, password, cb){
	Verification.findOne({userId: userId, utility:2, code: token}).exec(function(err, suc){
		if(err) {
			sails.log.error(err);
			return cb(err, null);
		}

		if(!suc)
			return cb({errorMessage: "Unidentified Link!!", errorCode: "ACCESS_DENIED"}, null);

		var createdAt = new Date(suc.createdAt).getTime()/1000;
		var curr = new Date().getTime()/1000;

		//1hr expiration!
		if(curr - createdAt < 3600)
		User.update({id: userId}, {password: password, reset: 1}).exec(function(err, user){
			if(err) {
				sails.log.error(err);
				return cb(err, null);
			}

			Verification.destroy({userId: userId, utility: 2}).exec(function(err, des){
				if(err) {
					sails.log.error(err);
					// return cb(err, null);
				}

				return cb(null, "password changed!");
			});
		});

		else
		Verification.destroy({userId: userId, utility: 2}).exec(function(err, des){
			if(err) {
				sails.log.error(err);
				// return cb(err, null);
			}

			return cb({errorMessage: "Expired Link!!", errorCode: "ACCESS_DENIED"}, null);
		});
	});
}

exports.signOut = function(userId, token, cb){
	RedisService.del(token, function(err, tok){
		if(err)
			return cb(err, null);

		return cb(null, true);
	});
}