var redis = require("redis");
//redis://redistogo:@:11149/
var client = redis.createClient(11149, 'barreleye.redistogo.com', {});

client.auth("71fd70c3e1333ea8e7185eb9c856263c", function (err) {
	if(err){
		sails.log.error(err);
	}
});

client.on("error", function (err) {
	console.log("Error creating redis client : " + err);
});

client.select(0, function (err, res) {
	if(err)
		console.log("Unable to select db 0 : " + err);
});

exports.set = function(key, value, collegeId, cb){
	client.set(key , value+ ' '+new Date().getTime().toString()+' '+collegeId, function(err, isSet){
		if(err)
			return cb(err, null);
		else
			return cb(null, isSet);
	});
};

exports.get = function(key, cb){
	client.get(key, function(err, value){
		if(err)
			return cb(err, null);
		else
			return cb(null, value);
	});
}

exports.del = function(key, cb){
	client.del(key, function(err, value){
		if(err)
			return cb(err, null);
		else
			return cb(null, value);
	});
}
