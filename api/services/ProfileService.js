exports.createUserProfile = function(userId, id, collegeId, categoryId, cb){
	User.findOne({id: id}, {collegeId: 1}).exec(function(err, user){
		if(err){
			sails.log.error(err);
			return cb(err, null);
		}

		if(!user)
			return cb({errorMessage: "This user does not exists", errorCode: "USER_NOT_FOUND"}, null);

		if(user.collegeId != collegeId)
			return cb({errorMessage: "Your college differ", errorCode: "NOT_ALLOWED"}, null);

		Category.findOne({id: categoryId}).exec(function(err, category){
			if(err){
				sails.log.error(err);
				return cb(err, null);
			}

			if(!category)
				return cb({errorMessage: "Unknown category", errorCode: "NOT_FOUND"}, null);

			Profile.findOne({userId: id, categoryId: categoryId}).exec(function(err, profile){
				if(err){
					sails.log.error(err);
					return cb(err, null);
				}

				if(profile)
					return cb({errorMessage: "This category to this user has already been added", errorCode: "DUPLICATE_NOT_ALLOWED"}, null);

				Profile.create({userId: id, userIdCreatedBy: userId, categoryId: categoryId, collegeId: collegeId}).exec(function(err, createdProfile){
					if(err){
						sails.log.info(err);
						return cb(err, null);
					}
					Activity.create({profileId: createdProfile.id, userIdFrom: userId, action: 0, collegeId: collegeId}).exec(function(err, activity){
						if(err)
							sails.log.error(err)
					});

					return cb(null, createdProfile);
				});
			});
		});
	});
}

/*
 * Cannot determine the race condition in profile count update!!
 */
exports.upvote = function(profileId, collegeId, userId, cb){
	Profile.findOne({id: profileId}).exec(function(err, profile){
		if(err){
			sails.log.error(err);
			return cb(err, null);
		}

		if(!profile)
			return cb({errorMessage: "This profile does not exists", errorCode: "PROFILE_NOT_FOUND"}, null);

		/* Lets say anybody can upvote anyone ! */
		Activity.findOne({profileId: profileId, userIdFrom: userId, action: 1}).exec(function(err, activity){
			if(err){
				sails.log.error(err);
				return cb(err, null);
			}

			if(activity)
				return cb({errorMessage: "You have already upvoted this", errorCode: "NOT_ALLOWED"}, null);

			Activity.create({profileId: profileId, userIdFrom: userId, action: 1, collegeId: collegeId}).exec(function(err, newActivity){
				if(err){
					sails.log.error(err);
					return cb(err, null);
				}

				//$inc not working
				//Profile.update({id: profileId}, { '$inc': {upvoteCount: 1}}).exec(function(err, updatedProfile){
				Profile.update({id: profileId}, {upvoteCount: profile.upvoteCount+1}).exec(function(err, updatedProfile){
					if(err){
						sails.log.error(err);
						return cb(err, null);
					}
					return cb(null, updatedProfile);
				});
			});
		});
	});
}