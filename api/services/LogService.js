var fs = require('fs');
var path = require('path');

var today = new Date().toISOString().split('T')[0];
var filePath = path.join(__dirname + '/../../log/'+ today+'.log');
console.log(filePath);

/*
 * This functions work is to log all the requests for which it has been called.
 * The result is independent of it's callback.
 */
exports.logEvent = function(req, status, cb){
	// sails.log.info(filePath);
	var toWrite = {status: status, request_time: req.param('request_received_time'),
		response_time: new Date().getTime(), id: req.param('requestId'),
		method: req.method, host: req.headers.host,
		url: req.url}
	fs.appendFile(filePath, JSON.stringify(toWrite).toString(), function(err,data){
		if(err){
			sails.log.error(err);
			return cb(err,null);
		}
		else{
			return cb(null, data);
		}
	});
}