exports.getRecent = function(collegeId, page, cb){
	Activity.find({where: {collegeId: collegeId}, sort: 'updatedAt DESC'}).paginate({page: page, limit: 10}).exec(function(err, activities){
		if(err){
			sails.log.error(err);
			return cb(err, null);
		}
		var data = [];
		// sails.log.info(activities.length);
		/*
		 * When you are fked. you write code with multiple callbacks.
		 */
		for(var i=0; i<activities.length; i++){
			(function(data, i){
				Profile.findOne({id: activities[i].profileId}).exec(function(err, profile){
					if(err)
						return cb(err, null);
					profile.getUserDetail(function(err, user1){
						if(err){
							sails.log.error(err);
							return cb(err, null);
						}
						profile.getUserCreatedDetail(function(err, user2){
							if(err){
								sails.log.error(err);
								return cb(err, null);
							}
							activities[i].getuserDetail(function(err, user3){
								if(err){
									sails.log.error(err);
									return cb(err, null);
								}
								profile.getCategoryDetail(function(err, category){
									if(err){
										sails.log.error(err);
										return cb(err, null);
									}
									var temp = {};
									temp.profileId = profile.id;
									temp.upvoteCount = profile.upvoteCount;
									temp.user = user1;
									temp.userCreatedBy = user2;
									if(activities[i].action == 0){
										temp.text = user2.firstname+" "+user2.lastname+" added "+user1.firstname+" "+user1.lastname+" as a "+category.title;
										temp.upvoteLink = "/profile/upvote?profileId="+profile.id;
										temp.time = profile.updatedAt;
										// sails.log.info("Pushing for 0");
										data.push(temp);
									}

									if(activities[i].action == 1){
										temp.userUpvotedBy = user3;
										temp.text = user3.firstname+" "+user3.lastname+" upvoted "+user1.firstname+" "+user1.lastname+" as a "+category.title;
										temp.upvoteLink = "/profile/upvote?profileId="+profile.id;
										temp.time = profile.updatedAt;
										// sails.log.info("Pushing for 1");
										data.push(temp);
									}

									recentActivity(data, activities.length-1, function(err, ans){
										if(ans)
											return cb(null, ans);
									});
								});
							});
						});
					});
					
				});
			})(data, i);
		}
	});
}

recentActivity = function(data, expected, cb){
	if(data.length == expected)
		return cb(null, data);
	else
		return cb(data, null);
}