/**
 * isAuthenticated
 *
 * @module      :: Policy
 * @description :: Simple policy to check if user is authenticated!
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {
	if(!req.headers.authorization) {
		sails.log.info(req.headers.authorization);
		return res.send({errorMessage: "Please provide authorization headers in correct format", errorCode: "INVALID_AUTHORIZATION_HEADER"},403);
	}

	var data = req.headers.authorization;
	RedisService.get(data, function(err,result){
		if(err) {
			LogService.logEvent(req, '500', function(err,cb){});
			return res.send({errorMessage: "Error in redis connection !!", errorCode: "REDIS_ERROR"}, 500);
		}

		if(!result){
			LogService.logEvent(req, '403', function(err,cb){});
			return res.send({errorMessage: "Not signed in", errorCode: "INVALID_REQUEST"}, 403);
		}

		result = result.split(' ');
		if(result[0]){
			if(!req.body)
				req.body = {};
			req.body.userId = result[0];
			req.body.collegeId = result[2];

			//Token expires in 7 days!
			if(new Date().getTime() - parseInt(result[1]) < 604800000)
				return next();
			else {
				LogService.logEvent(req, '403', function(err,cb){});
				RedisService.del(data, function(err, noErr){});
				return res.send({errorMessage: "Token expired!! Login again!", errorCode: "INVALID_TOKEN"}, 403);
			}
		}
		else{
			LogService.logEvent(req, '403', function(err,cb){});
			return res.send({errorMessage: "Not signed in", errorCode: "INVALID_TOKEN"}, 403);
		}
	});
};
