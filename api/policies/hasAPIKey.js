/**
 * hasAPIKey
 *
 * @module      :: Policy
 * @description :: Checking Api key. Will only be used during signup and signin!
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */

 module.exports = function(req, res, next) {
   

  if(!req.headers.authorization) {
    LogService.logEvent(req, '403', function(err,cb){});
    return res.send({errorMessage: "Please provide authorization headers in correct format", errorCode: "INVALID_AUTHORIZATION_HEADER"},403);
  }

  var data = req.headers.authorization;
  if( data == "92b6159c-1888-4fe6-af9c-8b26b2f63c5c")
    return next();
  else
    return res.send({errorMessage: "UnAuthorized", errorCode: "INVALID_AUTHORIZATION_HEADER"}, 403);
};