/**
* Category.js
*
* @description :: More items can be added here. -_-.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	title: {
  		type: 'STRING',
  		required: true,
  		unique: true
  	},

  	image: {
		type:'STRING'
	}
  }
};

