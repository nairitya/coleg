/**
* Profile.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	userId: {
  		type: 'STRING',
  		required: true
  	},

  	userIdCreatedBy: {
  		type: 'STRING',
  		required: true
  	},

  	categoryId: {
  		type: 'STRING',
  		required: true
  	},

  	upvoteCount: {
  		type: 'INTEGER',
  		defaultsTo: 0
  	},

    collegeId: {
      type: 'STRING',
      required: true
    },

    image: {
      type:'STRING'
    },

  	getCategoryDetail: function(cb){
  		var obj = this.toObject();
  		Category.findOne({id: this.categoryId}).exec(function(err, category){
  			if(err){
  				sails.log.error(err);
  				return cb(err, null);
  			}

  			return cb(null, category);
  		});
  	},

  	getUserDetail: function(cb){
  		var obj = this.toObject();
  		User.findOne({id: this.userId}).exec(function(err, user){
  			if(err){
  				sails.log.error(err);
  				return cb(err, null);
  			}

  			return cb(null, user.publicProfile());
  		});
  	},

  	getUserCreatedDetail: function(cb){
  		var obj = this.toObject();
  		User.findOne({id: this.userIdCreatedBy}).exec(function(err, user){
  			if(err){
  				sails.log.error(err);
  				return cb(err, null);
  			}

  			return cb(null, user.publicProfile());
  		});
  	}
  }
};

