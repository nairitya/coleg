/**
* User.js
*
* @description :: User deatilslislsiouaghv[iojghioerugn[aeorit[ vioae[tou h[eughpi ug [ourgorughogus]]]]]]
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var bcrypt = require('bcrypt');
module.exports = {

	attributes: {

		firstname: {
			type: 'STRING',
			required: false
		},

		lastname: {
			type: 'STRING',
			required: false
		},

		image: {
			type:'STRING',
			defaultsTo: 'https://lh3.googleusercontent.com/-MUnp3JDzzNc/AAAAAAAAAAI/AAAAAAAAALs/9RnSOo7YtVM/photo.jpg'
		},

		email: {
			type: 'email',
			required: true
		},

		collegeName: {
			type: 'STRING',
			required: false
		},

		collegeId:{
			type: 'STRING',
			required: true
		},

		username: {
			type: 'STRING',
			required: true,
			unique: true
		},

		password: {
			type: 'STRING',
			minLength: 4,
			columnName: 'encrypted_password'
		},

		gcmRegistrationId: {
			type: 'STRING',
			defaultsTo: 'not_found',
			required: false
		},

		gender: {
			type: 'STRING',
			enum: ['male','female', 'none'],
			defaultsTo: 'none'
		},

		graduationYear: {
			type: 'INTEGER'
		},

		enrollmentNumber: {
			type: 'STRING'
		},

		isEmailVerified:{
			type: 'BOOLEAN',
			defaultsTo: false
		},

		birthday: {
			type: 'datetime',
			required: false
		},

		github: {
			type: 'STRING'
		},

		bitbucket: {
			type: 'STRING'
		},

		twitter: {
			type: 'STRING'
		},

		facebook: {
			type: 'STRING'
		},

		publicProfile: function() {
			var obj = this.toObject();
			if(obj.github)
				obj.github = 'https://github.com/'+obj.github;
			if(obj.facebook)
				obj.facebook = 'https://facebook.com/'+obj.facebook;
			if(obj.twitter)
				obj.twitter = 'https://twitter.com/'+obj.twitter;
			if(obj.bitbucket)
				obj.bitbucket = 'https://bitbucket.com/'+obj.bitbucket;

			delete obj.password;
			delete obj.createdAt;
			delete obj.updatedAt;
			delete obj.gcmRegistrationId;
			delete obj.isEmailVerified;
			delete obj.email;

			obj.user = true; //In that way, it will never be null;
			return obj;
    	}
	},

	beforeCreate: function(values, next) {
		bcrypt.hash(values.password, 10, function(err, hash) {
			if(err) return next(err);
			values.password = hash;
			next();
		});
	},

	beforeUpdate: function(values, next){
		if(values.password)
			if(!values.reset)
				delete values.password;
			else
				delete values.reset;

		if(values.email)
			delete values.email;
		if(values.collegeId)
			delete values.collegeId;

		if(values.isEmailVerified)
			if(values.____verify__Email__True == sails.config.checker.secretCode)
				delete values.____verify__Email__True;
			else
				delete values.isEmailVerified;
		if(values.collegeName)
			delete values.collegeName;
		if(values.username)
			delete values.username;

		// sails.log.info("Updating the user!!");
		if(values.password)
			bcrypt.hash(values.password, 10, function(err, hash) {
				if(err) 
					return next(err);
				values.password = hash;
				next();
			});
		else
			next();
	}
};

