/**
* Activity.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
* action == 1 --> Upvote
* action == 2 --> Url sharing
* action == 0 --> Added a new profile for a user.
*/

module.exports = {

  attributes: {
  	profileId: {
  		type: 'STRING',
  		required: false
  	},

  	userIdFrom: {
  		type: 'STRING',
  		required: true
  	},

  	action: {
  		type: 'INTEGER',
  		required: true
  	},

  	url: {
  		type: 'STRING',
  		required: false
  	},

    collegeId: {
      type: 'STRING',
      required: true
    },

    getuserDetail: function(cb){
      var obj = this.toObject();
      User.findOne({id: this.userIdFrom}).exec(function(err, user){
        if(err){
          sails.log.error(err);
          return cb(err, null);
        }

        return cb(null, user.publicProfile());
      });
    }

  	// text: {
  	// 	type: 'STRING'
  	// }
  },

  beforeCreate: function(values, next){
  	next();
  }
};

