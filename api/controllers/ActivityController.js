/**
 * ActivityController
 *
 * @description :: Server-side logic for managing Activities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	_config: {
		model: "activity"
	},

	/*
	 * Hugely needs modification.
	 */
	recent: function(req, res){

		sails.log("TIME: "+ new Date()+ ": "
		    +req.method+ " " +req.headers.host+" " +req.url+ " "
		    +JSON.stringify(req.params)+" "+req.route.params
		);

		var collegeId = req.param('collegeId');
		var page = req.param('page');
		if(!page)
			page = 1;
		else
			try{
				page = parseInt(page);
			}
			catch(e){
				page = 1;
			}

		ActivityService.getRecent(collegeId, page, function(err, activities){
			if(err){
				LogService.logEvent(req, '400', function(err,cb){});
				return res.send(err, 400);
			}

			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(activities, 200);
		});
	}
};

