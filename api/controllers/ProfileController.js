/**
 * ProfileController
 *
 * @description :: Server-side logic for managing Profiles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	_config: {
		model: 'profile'
	},

	adduserprofile: function(req, res){
    	sails.log("TIME: "+ new Date()+ ": "
		    +req.method+ " " +req.headers.host+" " +req.url+ " "
		    +JSON.stringify(req.params)+" "+req.route.params
		);

    	var userId = req.param('userId');
    	var id = req.param('id');
    	var collegeId = req.param('collegeId');
    	var categoryId = req.param('categoryId');

    	if(!id)
    		return res.send({errorMessage: "select a user first", errorCode: "VALIDATION_ERROR"}, 400);
    	if(!categoryId)
    		return res.send({errorMessage: "select a category", errorCode: "VALIDATION_ERROR"}, 400);

    	ProfileService.createUserProfile(userId, id, collegeId, categoryId, function(err, profile){
    		if(err){
				LogService.logEvent(req, '400', function(err,cb){});
				return res.send(err, 400);
			}

			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(profile, 200);
    	});
    },

    upvote: function(req, res){

		sails.log("TIME: "+ new Date()+ ": "
		    +req.method+ " " +req.headers.host+" " +req.url+ " "
		    +JSON.stringify(req.params)+" "+req.route.params
		);

		var profileId = req.param('profileId');
		var collegeId = req.param('collegeId');
		var userId = req.param('userId');

		if(!profileId)
			return res.send({errorMessage: "ProfileId missing", errorCode: "VALIDATION_ERROR"}, 400);

		ProfileService.upvote(profileId, collegeId, userId, function(err, profile){
			if(err){
				LogService.logEvent(req, '400', function(err,cb){});
				return res.send(err, 400);
			}

			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(profile, 200);
		});
	},

	top: function(req, res){
    	sails.log("TIME: "+ new Date()+ ": "
		    +req.method+ " " +req.headers.host+" " +req.url+ " "
		    +JSON.stringify(req.params)+" "+req.route.params
		);

		var page = req.param('page');
		if(!page)
			page = 1;
		else
			try{
				page = parseInt(page);
			}
			catch(e){
				page = 1;
			}

		var categoryId = req.param('categoryId');
		if(!categoryId)
			return res.send({errorMessage: "Category required", errorCode: "VALIDATION_ERROR"}, 400);

		var cId = req.param('cId');
		if(!cId)
			cId = req.param('collegeId');

		UserService.getTop(categoryId, cId, page, 10, function(err, users){
			if(err){
				LogService.logEvent(req, '400', function(err,cb){});
				return res.send(err, 400);
			}

			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(users, 200);
		});
    },

    topall: function(req, res){
    	sails.log("TIME: "+ new Date()+ ": "
		    +req.method+ " " +req.headers.host+" " +req.url+ " "
		    +JSON.stringify(req.params)+" "+req.route.params
		);

		var cId = req.param('cId');
		if(!cId)
			cId = req.param('collegeId');

		UserService.getTopFromAllCategory(cId, function(err, users){
			if(err){
				LogService.logEvent(req, '400', function(err,cb){});
				return res.send(err, 400);
			}

			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(users, 200);
		});
    }
};

