/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	_config: {
		model: 'user'
	},

	signup: function(req, res){

		sails.log("TIME: "+ new Date()+ ": "
	        +req.method+ " " +req.headers.host+" " +req.url+ " "
	        +JSON.stringify(req.params)+" "+req.route.params
	    );

	    var username = req.param('username');
	  	var password = req.param('password');

	  	var firstname = req.param('firstname');
	  	var lastname = req.param('lastname');
	  	var email = req.param('email');
	  	var collegeId = req.param('collegeId');
	  	var gcmRegistrationId = req.param('gcmRegistrationId');

	  	if(!username){
	  	  LogService.logEvent(req, '400', function(err,cb){});
	      return res.send({errorMessage : 'username is required', errorCode: "VALIDATION_ERROR"},400);
	  	}

	  	if(!firstname){
	  	  LogService.logEvent(req, '400', function(err,cb){});
	      return res.send({errorMessage : 'firstname is required', errorCode: "VALIDATION_ERROR"},400);
	  	}
	  	if(!password) {
	      LogService.logEvent(req, '400', function(err,cb){});
	      return res.send({errorMessage: 'password is required', errorCode: "VALIDATION_ERROR"}, 400);
	    }
	    if(!email){
	      LogService.logEvent(req, '400', function(err,cb){});
	      return res.send({errorMessage: 'email is required', errorCode: "VALIDATION_ERROR"}, 400);
	    }
	    if(!collegeId){
	      LogService.logEvent(req, '400', function(err,cb){});
	      return res.send({errorMessage: 'collegeId is required', errorCode: "VALIDATION_ERROR"}, 400);
	    }

	  	if(!gcmRegistrationId) {
	      gcmRegistrationId = "NOT_FOUND";
	    }

	    var email_reg = /^([\w-]+(?:\.[\w-]+)*)@iit((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i; //"

		if(!email_reg.test(email))
			return res.send({errorMessage: "Invalid email, supported only for IIT students", errorCode: "VALIDATION_ERROR"}, 400);
		
	  	SignupService.signUpUser(username, password, email, firstname, lastname, collegeId, gcmRegistrationId, function(err,result){
	  		if(err){
				LogService.logEvent(req, '500', function(err,cb){});
				return res.send(err,500);
			}
			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(result, 200);
	  	});
	},

	signin: function(req, res){

		sails.log("TIME: "+ new Date()+ ": "
			+req.method+ " " +req.headers.host+" " +req.url+ " "
			+JSON.stringify(req.params)+" "+req.route.params
		);

		var username = req.param('username');
		var password = req.param('password');

		if(!username || !password) {
			LogService.logEvent(req, '400', function(err,cb){});
			return res.send({errorMessage: "Some credentials Missing !!", errorCode: "VALIDATION_ERROR"}, 400);
		}

		SignupService.signin(username, password, function(err, result){
			if(err){
				LogService.logEvent(req, '403', function(err,cb){});
				return res.send(err, 403);
			}
			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(result, 200);
		});
	},

	emailVerification: function(req, res){
		sails.log("TIME: "+ new Date()+ ": "
		    +req.method+ " " +req.headers.host+" " +req.url+ " "
		    +JSON.stringify(req.params)+" "+req.route.params
		);

		var userId = req.param('userId');
		var token = req.param('token');
		if(!userId || !token)
		  return res.json({
			errorMessage: "Query missing!",
			errorCode: "VALIDATION_ERROR"
		  });

		SignupService.verifyEmail(userId, token, function(err, verify){
			if(err){
				LogService.logEvent(req, '403', function(err,cb){});
				return res.send(err, 403);
			}

			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(verify, 200);
		});
    },

    search: function(req, res){
    	sails.log("TIME: "+ new Date()+ ": "
		    +req.method+ " " +req.headers.host+" " +req.url+ " "
		    +JSON.stringify(req.params)+" "+req.route.params
		);

		var page = req.param('page');
		if(!page)
			page = 1;
		else
			try{
				page = parseInt(page);
			}
			catch(e){
				page = 1;
			}

		var userId = req.param('userId');
		var collegeId = req.param('collegeId');
		var name = req.param('name');

		if(!name)
			return res.send([], 200);

		UserService.findUserByName(userId, collegeId, name, page, function(err, user){
			if(err){
				LogService.logEvent(req, '400', function(err,cb){});
				return res.send(err, 400);
			}

			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(user, 200);
		});
    },

    find: function(req, res){
    	sails.log("TIME: "+ new Date()+ ": "
		    +req.method+ " " +req.headers.host+" " +req.url+ " "
		    +JSON.stringify(req.params)+" "+req.route.params
		);

		var id = req.param('id');
		if(!id)
			id = req.param('userId');

		UserService.getUserPublicProfile(id, function(err, users){
			if(err){
				LogService.logEvent(req, '400', function(err,cb){});
				return res.send(err, 400);
			}

			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(users, 200);
		});
    },

    signout: function(req, res){

		sails.log("TIME: "+ new Date()+ ": "
	        +req.method+ " " +req.headers.host+" " +req.url+ " "
	        +JSON.stringify(req.params)+" "+req.route.params
	    );

	    var token = req.headers.authorization;
	    if(!token)
	    	return res.send({errorMessage: "Missing token", errorCode: "VALIDATION_ERROR"}, 400);

	    var userId = req.param('userId');

	    SignupService.signOut(userId, token, function(err, user){
	    	if(err){
				LogService.logEvent(req, '400', function(err,cb){});
				return res.send(err, 400);
			}
			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(user, 200);
	    });
	},

	sendResetMail: function(req, res){
		sails.log("TIME: "+ new Date()+ ": "
			+req.method+ " " +req.headers.host+" " +req.url+ " "
			+JSON.stringify(req.params)+" "+req.route.params
		);

		var username = req.param('username');
		if(!username)
			return res.json({
			errorMessage: "Query missing!",
			errorCode: "VALIDATION_ERROR"
		});

		SignupService.sendResetEmail(username, function(err, em){
			if(err){
				LogService.logEvent(req, '403', function(err,cb){});
				return res.send(err, 403);
			}

			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(em, 200);
		});
	},

	renderResetPage: function(req, res){

		sails.log("TIME: "+ new Date()+ ": "
			+req.method+ " " +req.headers.host+" " +req.url+ " "
			+JSON.stringify(req.params)+" "+req.route.params
		);

		var userId = req.param('userId');
		var token = req.param('token');

		if(!userId || !token)
		return res.json({
			errorMessage: "Query missing!",
			errorCode: "VALIDATION_ERROR"
		});

		return res.view('reset', { 
			userId: userId, token: token,
			layout: null
		});
	},

	resetPassword: function(req, res){

		sails.log("TIME: "+ new Date()+ ": "
			+req.method+ " " +req.headers.host+" " +req.url+ " "
			+JSON.stringify(req.params)+" "+req.route.params
		);

		var userId = req.param('userId');
		var token = req.param('token');
		var password = req.param('password');
		var confirm = req.param('confirm');

		if(!userId || !token || !password || !confirm)
		return res.json({
			errorMessage: "Query missing!",
			errorCode: "VALIDATION_ERROR"
		});

		if(password != confirm)
		return res.json({
			errorMessage: "password mismatch!",
			errorCode: "VALIDATION_ERROR"
		});

		SignupService.resetUserPassword(userId, token, password, function(err, changed){
			if(err){
				LogService.logEvent(req, '403', function(err,cb){});
				return res.send(err, 403);
			}

			LogService.logEvent(req, '200', function(err,cb){});
			return res.send(changed, 200);
		});
	},
};

