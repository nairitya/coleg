/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

  UserController: {
    signup: 'hasAPIKey',
    signin: 'hasAPIKey',
    search: 'isAuthenticated',
    addUserProfile: 'isAuthenticated',
    create: false,
    update: 'isAuthenticated',
    find: 'isAuthenticated',
    sendResetMail: 'hasAPIKey',
    renderResetPage: true,
    resetPassword: true,
    emailVerification: true,
    '*': false
  },

  ProfileController: {
  	adduserprofile: 'isAuthenticated',
  	upvote: 'isAuthenticated',
    find: 'isAuthenticatedForProfile',
    top: 'isAuthenticated',
    topall: 'isAuthenticated',
  	'*': false
  },

  ActivityController: {
  	recent: 'isAuthenticated',
  	'*': false
  },

  CollegeController: {
    find: 'hasAPIKey',
    '*': true
  },

  CategoryController: {
    find: 'hasAPIKey',
    '*': true
  }

};
