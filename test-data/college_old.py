college = """
[
	{
		"name": "Indian Institute of Technology, Roorkee",
		"shortCode": "IITR",
		"emailIdFormat": "@iitr",
		"image": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Main_administrative_building_of_IIT_Roorkee.jpg/640px-Main_administrative_building_of_IIT_Roorkee.jpg"
	},
	{
		"name": "Indian Institute of Technology, Kharagpur",
		"shortCode": "IITKGP",
		"emailIdFormat": "@iitkgp",
		"image": "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Hijli_Shaheed_Bhawan.JPG/640px-Hijli_Shaheed_Bhawan.JPG"
	},
	{
		"name": "Indian Institute of Technology, Bombay",
		"shortCode": "IITB",
		"emailIdFormat": "@iitb",
		"image": "https://upload.wikimedia.org/wikipedia/commons/0/05/IITB_Main_Building.jpg"
	},
	{
		"name": "Indian Institute of Technology, Delhi",
		"shortCode": "IITD",
		"emailIdFormat": "@iitd",
		"image": "https://upload.wikimedia.org/wikipedia/commons/5/51/IITDelhiMath.jpg"
	},
	{
		"name": "Indian Institute of Technology, Kanpur",
		"shortCode": "IITK",
		"emailIdFormat": "@iitk",
		"image": "https://upload.wikimedia.org/wikipedia/commons/0/03/IITKLibrary.jpg"
	},
	{
		"name": "Indian Institute of Technology, Guwahati",
		"shortCode": "IITG",
		"emailIdFormat": "@iitg",
		"image": "https://upload.wikimedia.org/wikipedia/commons/c/cd/Iitg_from_a_distance.jpg"
	},
	{
		"name": "Indian Institute of Technology, Madras",
		"shortCode": "IITM",
		"emailIdFormat": "@iitm",
		"image": "https://upload.wikimedia.org/wikipedia/commons/2/2b/IITM_Library.JPG"
	}
]
"""

import requests as r
import json
data = json.loads(college)
for a in data:
    r.post('http://coleg.eu-gb.mybluemix.net/college', data=a)