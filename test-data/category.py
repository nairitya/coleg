category = """
[
    {
        "title": "Handsome",
        "image": "https://upload.wikimedia.org/wikipedia/commons/3/3b/Handsome_Hermes.jpg"
    },
    {
        "title": "Hacker",
        "image": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Nuvola-inspired-terminal.svg/1205px-Nuvola-inspired-terminal.svg.png"
    },
    {
        "title": "Dancer",
        "image": "https://upload.wikimedia.org/wikipedia/commons/f/f7/Dancer_Silhouette_One.png"
    },
    {
        "title": "Actor",
        "image": "http://png-1.findicons.com/files/icons/2711/free_icons_for_windows8_metro/128/theatre_masks.png"
    },
    {
        "title": "Designer",
        "image": "https://upload.wikimedia.org/wikipedia/commons/0/08/Pencils_hb.jpg"
    },
    {
        "title": "Photographer",
        "image": "https://upload.wikimedia.org/wikipedia/commons/9/97/Camera-icon.png"
    },
    {
        "title": "Singer",
        "image": "https://upload.wikimedia.org/wikipedia/commons/1/1c/Female_singer_silhouette.png"
    },
    {
        "title": "Anchor",
        "image": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQtqdgFXvtE9vVYNMZeGTjrBS-8D6txQN2QQTUp_vc2njRXHnPqNA"
    },
    {
        "title": "Beautiful",
        "image": "http://upload.wikimedia.org/wikipedia/commons/f/f3/Eternal-question.gif"
    },
    {
        "title": "Writer",
        "image": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Inkwell_icon_-_Noun_Project_2512.svg/1024px-Inkwell_icon_-_Noun_Project_2512.svg.png"
    },
    {
        "title": "Chef",
        "image": "https://pixabay.com/static/uploads/photo/2014/04/03/00/42/chef-hat-309146_640.png"
    }
]
"""

import requests as r
import json
data = json.loads(category)
for a in data:
    r.post('http://coleg.eu-gb.mybluemix.net/category', data=a)